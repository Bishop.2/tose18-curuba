package ch.briggen.bfh.sparklist.domain;


/**
 * Einzelner Eintrag in der Liste mit Name und Menge (und einer eindeutigen Id)
 * @author Marcel Briggen
 *
 */
public class Stakeholder {
	
	private long id;
	private String name;
	private String vorname;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Stakeholder()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param name Name des eintrags in der Liste
	 * @param quantity Menge
	 */
	public Stakeholder(long id, String name, String vorname)
	{
		this.id = id;
		this.name = name;
		this.vorname = vorname;
	}

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVorname() {
		return name;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	@Override
	public String toString() {
		return String.format("Stakeholder:{id: %d; name: %s; vorname: %d;}", id, name, vorname);
	}
	

}
