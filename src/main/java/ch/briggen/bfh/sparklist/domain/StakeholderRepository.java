package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Repository für alle Items. 
 * Hier werden alle Funktionen für die DB-Operationen zu Items implementiert
 * @author Marcel Briggen
 *
 */


public class StakeholderRepository {
	
	private final Logger log = LoggerFactory.getLogger(ItemRepository.class);
	

	/**
	 * Liefert alle items in der Datenbank
	 * @return Collection aller Items
	 */
	public Collection<Stakeholder> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select name, vorname from stakeholder");
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all items. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	
	
	/**
	 * Helper zum konvertieren der Resultsets in Item-Objekte. Siehe getByXXX Methoden.
	 * @author Marcel Briggen
	 * @throws SQLException 
	 *
	 */
	private static Collection<Stakeholder> mapItems(ResultSet rs) throws SQLException 
	{
		LinkedList<Stakeholder> list = new LinkedList<Stakeholder>();
		while(rs.next())
		{
			Stakeholder i = new Stakeholder(rs.getLong("id"),rs.getString("name"),rs.getString("vorname"));
			list.add(i);
		}
		return list;
	}

}
